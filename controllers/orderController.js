const bcrypt = require('bcrypt');

const Order = require('../models/order');
const Product = require('../models/product');
const auth = require('../auth');

//Create Order
module.exports.createOrder = async (reqBody, userId) => {
	let product = await Product.findById(reqBody.productId).then(result => {return result});
    let orderSubtotal = product.price * reqBody.orderQty;
    let userOrder = {
    	productId: reqBody.productId,
    	name: product.name,
    	orderQty: reqBody.orderQty,
    	subtotal: orderSubtotal
    }

    return Order.findOne({userId: userId, status: "onCart"}).then(result => {
    	if(result === null) {
    		let newOrder = new Order({
    			totalAmount: orderSubtotal,
    			userId: userId,
    			orders: userOrder
    		})

    		return newOrder.save().then((order, err) => {
    			if(err) {
    				return false;
    			} else {
    				return {msg: "newCart"};
    			}
    		})
    	} else {
    		let isOnCart = false;
    		let index;

    		for(let i = 0; i < result.orders.length; i++) {
    			if(result.orders[i].productId === reqBody.productId) {
    				isOnCart = true;
    				index = i;
    				suborderId = result.orders[i]._id;
    			};
    		}

    		if(isOnCart) {
    			let updatedCart = [];

    			for(let i = 0; i < result.orders.length; i++) {
    				if(i === index) {
    					let updatedOrder = {
    						productId: reqBody.productId,
    						name: product.name,
    						orderQty: reqBody.orderQty,
    						subtotal: orderSubtotal
    					};

    					updatedCart.push(updatedOrder);

    				} else {
    					updatedCart.push(result.orders[i]);
    				};
    			};

    			let update = {
    				totalAmount: result.totalAmount - result.orders[index].subtotal + orderSubtotal,
    				purchasedOn: new Date(),
    				orders: updatedCart
    			};

    			return Order.findByIdAndUpdate(result._id, update).then((order, err) => {
    				if(err) {
    					return false;
    				} else {
    					return {msg: "updatedCart"};
    				};
    			});
    		} else {
    			let addOrder = {
    				productId: reqBody.productId,
    				name: product.name,
    				orderQty: reqBody.orderQty,
    				subtotal: orderSubtotal
    			}

    			let update = {
    				totalAmount: result.totalAmount + orderSubtotal,
    				purchasedOn: new Date()
    			}

    			return Order.findByIdAndUpdate(result._id, {$push: {orders: addOrder}}).then((order, err) => {
    				if(err) {
    					return false;
    				} else {
    					return Order.findByIdAndUpdate(result._id, update).then((order,err) => {
    						if(err) {
    							return false;
    						} else {
    							return {msg: "addedToCart"};
    						};
    					});
    				};
    			});
    		};
    	};
    });
};

//Retrieve All Orders
module.exports.retrieveAllOrders = () => {
	return Order.find({status: {$nin: "onCart"}}).then(result => {
		return result;
	});
};

//Retrieve User's Orders
module.exports.myOrder = (userId) => {
	return Order.find({userId: userId, status: {$nin: "onCart"}}).then(result => {
		return result;
	});
}

//Retrieve User's Cart
module.exports.myCart = (userId) => {
	return Order.find({userId: userId, status: "onCart"}).then(result => {
		return result;
	});
}

//Update Order to Processing
module.exports.checkout = (userId) => {
	return Order.findOneAndUpdate({userId: userId, status: "onCart"}, {status: "processing"}).then((result, err) => {
		if(err) {
			return false;
		} else {
			return true;
		}
	});
}

//Update Order to Completed
module.exports.orderComplete = (reqBody) => {
	return Order.findOneAndUpdate({_id: reqBody.orderId, status: "processing"}, {status: "completed"}).then((result, err) => {
		if(err) {
			return false;
		} else {
			return true;
		}
	}).catch(err => err);
}

//Remove from Cart
module.exports.removeFromCart = (reqBody, userId) => {
	return Order.findOne({userId: userId, status: "onCart"}).then((result, err) => {
		if(err) {
			return false;
		} else {
			let isOnCart = false;

			for(let i = 0; i < result.orders.length; i++) {
				if(result.orders[i].productId === reqBody.productId) {
					isOnCart = true
				};
			};

			if(isOnCart) {
				if(result.orders.length === 1) {
					return Order.findOneAndDelete({userId: userId, status: "onCart"}).then((result, err) => {
						if(err) {
							return false;
						} else {
							return true;
						};
					});
				} else {
					let updatedTotalAmount;
					let updatedOrders = [];

					for(let i = 0; i < result.orders.length; i++) {
						if(result.orders[i].productId !== reqBody.productId) {
							updatedOrders.push(result.orders[i])
						} else {
							updatedTotalAmount = result.totalAmount - result.orders[i].subtotal;
						};
					};

					let update = {
						totalAmount: updatedTotalAmount,
						purchasedOn: new Date(),
						orders: updatedOrders
					}

					return Order.findOneAndUpdate({userId: userId, status: "onCart"}, update).then((result, err) => {
						if(err) {
							return false;
						} else {
							return true;
						}
					});
				};
			} else {
				return false;
			};
		};
	});
}