const bcrypt = require('bcrypt');

const Product = require('../models/product');

//Add Products
module.exports.addProduct = (reqBody) => {
    let newProduct = new Product({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
        image: reqBody.image
    });

    return Product.findOne({name: newProduct.name}).then(result => {
		if(result !== null) {
			return {msg: 'duplicate'};
		} else {
		    return newProduct.save().then((result, error) => {
		        if (error) {
		            return false;
		        } else {
		            return true;
		        };
		    });
		 };
    }).catch(error => false);
};

//Get All Active Products
module.exports.getAllActiveProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	});
};

//Get All Active Products
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	});
};

//Get Specific Product
module.exports.getSpecificProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	});
};

//Update Product
module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		image: reqBody.image
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((result, err) => {
		if(err) {
			return false;
		} else {
			return true;
		};
	});
};

//Archive Product
module.exports.archiveProduct = (reqParams) => {
	let updatedProduct = {
		isActive: false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((result, err) => {
		if(err) {
			return `Archive product not successful.`;
		} else {
			return `Archive product successful.`;
		};
	});
};

//Unarchive Product
module.exports.unarchiveProduct = (reqParams) => {
	let updatedProduct = {
		isActive: true
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((result, err) => {
		if(err) {
			return `Archive product not successful.`;
		} else {
			return `Archive product successful.`;
		};
	});
};