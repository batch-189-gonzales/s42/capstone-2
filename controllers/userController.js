const bcrypt = require('bcrypt');

const User = require('../models/user');
const auth = require('../auth');

//Check Email
module.exports.checkEmailExist = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0) {
			return true;
		} else {
			return false;
		};
	});
};

//User Registration
module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10),
        mobileNo: reqBody.mobileNo
    });

    return newUser.save().then((user, error) => {
        if (error) {
            return false;
        } else {
            return true;
        };
    });
};

//User Login
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return {msg: 'user'};
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)};
			} else {
				return {msg: 'password'};
			};
		};
	});
};

//User Details
module.exports.getProfile = (data) => {
    return User.findById(data.id).then((result) => {
        if (result == null) {
            return false;
        } else {
            result.password = '';
            return result;
        };
    });
};

//Set User as Admin (Access: Admin only)
module.exports.userAdmin = (reqParams, reqBody) => {
	let updatedUser = {
		isAdmin: reqBody.isAdmin
	};

	return User.findByIdAndUpdate(reqParams.userId, updatedUser).then((user, err) => {
		if(err) {
			return `Set user as admin failed. Try again.`;
		} else {
			if(updatedUser.isAdmin) {
				return `Granted user admin access.`;
			} else {
				return `Revoked user admin access.`;
			}
		};
	});
};