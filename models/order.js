const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
	totalAmount: {
		type: Number
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	userId: {
		type: String,
		required: [true, "User ID is required"]
	},
	orders: [
		{
			productId: {
				type: String,
				required: [true, "Product ID is required"]
			},
			name: {
				type: String,
			},
			orderQty: {
				type: Number,
				required: [true, "Order quantity is required"]
			},
			subtotal: {
				type: Number
			}
		}
	],
	status: {
		type: String,
		default: "onCart"
	}
});

module.exports = mongoose.model("Order", orderSchema);