const express = require('express');
const router = express.Router();

const productController = require('../controllers/productController');
const auth = require('../auth');

//Add Products
router.post("/", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	//Admin access
	if(isAdmin) {
		productController.addProduct(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(`Admin permission required. Please login using admin account.`)
	};
});

//Get All Active Products
router.get("/", (req, res) => {
	productController.getAllActiveProducts().then(resultFromController => res.send(resultFromController));
});

//Get All Active Products
router.get("/admin", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	//Admin access
	if(isAdmin) {
		productController.getAllProducts().then(resultFromController => res.send(resultFromController));
	} else {
		res.send(`Admin permission required. Please login using admin account.`)
	};
});

//Get Specific Product
router.get("/:productId", (req, res) => {
	productController.getSpecificProduct(req.params).then(resultFromController => res.send(resultFromController));
});

//Update Product
router.put("/:productId", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	//Admin access
	if(isAdmin) {
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(`Admin permission required. Please login using admin account.`)
	};
});

//Archive Product
router.put("/:productId/archive", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	//Admin access
	if(isAdmin) {
		productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(`Admin permission required. Please login using admin account.`)
	};
});

//Unarchive Product
router.put("/:productId/unarchive", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	//Admin access
	if(isAdmin) {
		productController.unarchiveProduct(req.params).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(`Admin permission required. Please login using admin account.`)
	};
});

module.exports = router;