const express = require('express');
const router = express.Router();

const userController = require('../controllers/userController');
const orderController = require('../controllers/orderController')
const auth = require('../auth');

//Check Email
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController));
});

//User Registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

//User Login
router.post("/login", (req, res) => {
    userController.loginUser(req.body).then((resultFromController) => res.send(resultFromController));
});

//User Details
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

    userController.getProfile({id: userData.id}).then((resultFromController) => res.send(resultFromController));
});

//Set User as Admin (Access: Admin only)
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	//Admin access
	if(isAdmin) {
		userController.userAdmin(req.params, req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(`Admin permission required. Please login using admin account.`)
	};
});

//Create Order
router.post("/checkout", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	const userId = auth.decode(req.headers.authorization).id;

	//User access
	if(isAdmin) {
		res.send(`User access only. Please login using user account.`);
	} else {
		orderController.createOrder(req.body, userId).then(resultFromController => res.send(resultFromController));
	};
});

//Retrieve All Orders
router.get("/orders", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	//Admin access
	if(isAdmin) {
		orderController.retrieveAllOrders().then(resultFromController => res.send(resultFromController));
	} else {
		res.send(`Admin permission required. Please login using admin account.`)
	};
});

//Retrieve User's Orders
router.get("/myOrder", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	const userId = auth.decode(req.headers.authorization).id;

	//User access
	if(isAdmin) {
		res.send(`User access only. Please login using user account.`);
	} else {
		orderController.myOrder(userId).then(resultFromController => res.send(resultFromController));
	};
});

//Retrieve User's Cart
router.get("/myCart", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	const userId = auth.decode(req.headers.authorization).id;

	//User access
	if(isAdmin) {
		res.send(`User access only. Please login using user account.`);
	} else {
		orderController.myCart(userId).then(resultFromController => res.send(resultFromController));
	};
});

//Checkout Order
router.put("/checkout", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	const userId = auth.decode(req.headers.authorization).id;

	//User access
	if(isAdmin) {
		res.send(`User access only. Please login using user account.`);
	} else {
		orderController.checkout(userId).then(resultFromController => res.send(resultFromController));
	};
});

//Remove from Cart
router.post("/removeFromCart", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	const userId = auth.decode(req.headers.authorization).id;

	//User access
	if(isAdmin) {
		res.send(`User access only. Please login using user account.`);
	} else {
		orderController.removeFromCart(req.body, userId).then(resultFromController => res.send(resultFromController));
	};
});

//Update Order to Completed
router.put("/complete", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;

	//User access
	if(isAdmin) {
		orderController.orderComplete(req.body).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(`Admin permission required. Please login using admin account.`)
	};
});

module.exports = router;